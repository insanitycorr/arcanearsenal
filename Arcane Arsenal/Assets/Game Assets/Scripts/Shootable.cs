﻿
using UnityEngine;

public class Shootable : MonoBehaviour
{
	public float HP = 100f;

	public void TakeDamage(float dmgVal)
	{
		HP -= dmgVal;
		if (HP <= 0f)
		{
			Die();
		}
	}

	void Die()
	{
		Destroy(gameObject);
	}
}
