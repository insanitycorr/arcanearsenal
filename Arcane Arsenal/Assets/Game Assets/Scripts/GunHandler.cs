﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GunHandler : MonoBehaviour
{


	public Guns gun;
	public Guns slot1, slot2, slot3, slot4, slot5;

	public Camera PV;

	public ParticleSystem muzzleFlash;
	public ParticleSystem tracer;

	public GameObject bulletHole;
	public GameObject bulletImpact;

	public float accuracy;
	public float bulletStrength;
	public float dmg;
	public float RoF;
	public int clip;
	public int projNum;
	public int accReduc;

	public Transform ammoHUD;
	public Transform barrel;

	private int remainingClip;
	private float currentAcc;
	private float timeToNextShot;

	public AudioClip gunShot;
	public AudioClip dry;

	void Start()
	{
		updateWeapon();

		reload();

		updateAmmoHud();
	}

	void Update()
	{
		if (Input.GetKeyDown(Bindings.reload))
		{
			reload();
		}
		//Fires weapon, limited by firerate
		if (Input.GetKey(Bindings.attack0) && Time.time >= timeToNextShot)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;

			timeToNextShot = Time.time + 1f / RoF;
			if (remainingClip >= 1)
			{
				Fire();
			}
			else
			{
				dryFire();
			}
		}
		//weapon selection keys 1-5
		if (Input.GetKey(Bindings.inv1))
		{
			gun = slot1;
			updateWeapon();
		}

		if (Input.GetKey(Bindings.inv2))
		{
			gun = slot2;
			updateWeapon();
		}

		if (Input.GetKey(Bindings.inv3))
		{
			gun = slot3;
			updateWeapon();
		}

		if (Input.GetKey(Bindings.inv4))
		{
			gun = slot4;
			updateWeapon();
		}

		if (Input.GetKey(Bindings.inv5))
		{
			gun = slot5;
			updateWeapon();
		}
	}

	void Fire()
	{
		//plays muzzle flash and bullet VFX
		remainingClip--;
		updateAmmoHud();
		AudioSource.PlayClipAtPoint(gunShot, PV.transform.position, .3f);

		for (int i = 0; i < projNum; i++)
		{
			//Aimcone/shotgunSpread
			float variance = (200 - currentAcc) / 1000;
			Vector3 direction = PV.transform.forward;
			direction.x += Random.Range(-variance, variance);
			direction.y += Random.Range(-variance, variance);
			direction.z += Random.Range(-variance, variance);

			currentAcc = currentAcc - accReduc;

			if (currentAcc <= 0.0f)
				currentAcc = 0.0f;

			Ray ray = new Ray(PV.transform.position, direction);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				Debug.Log(hit);
				Shootable target = hit.transform.GetComponent<Shootable>();


				if (target != null)
				{
					//deals damage to damageable targets
					target.TakeDamage(dmg);
				}
				if (hit.rigidbody != null)
				{
					//adds force to shot rigidbodies
					hit.rigidbody.AddForce(-hit.normal * bulletStrength);
				}
				if (target == null)
				{
					GameObject hole = Instantiate(bulletHole, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
					Destroy(hole, 30f);
				}

				//Creates bullet impact particle system at point of impact
				ParticleSystem muzzle = Instantiate(muzzleFlash, barrel.position, Quaternion.LookRotation(direction));
				if (muzzle.IsAlive())
				{
					Destroy(muzzle.gameObject, 1f);
				}
				ParticleSystem shot = Instantiate(tracer, barrel.position, Quaternion.LookRotation(direction));
				if (shot.IsAlive())
				{
					Destroy(shot.gameObject,2f);
				}
				GameObject impact = Instantiate(bulletImpact, hit.point, Quaternion.LookRotation(hit.normal));
				Destroy(impact, 2f);

			}
		}
	}
			
	void updateAmmoHud()
	{
		ammoHUD.GetComponent<Text>().text = remainingClip.ToString();
		ammoHUD.GetComponent<Text>().text = ammoHUD.GetComponent<Text>().text + " / " + clip;
	}
	void dryFire()
	{
		AudioSource.PlayClipAtPoint(dry, PV.transform.position, .6f);
	}
	void reload()
	{
		remainingClip = clip;
		ammoHUD.GetComponent<Text>().text = remainingClip.ToString();
		ammoHUD.GetComponent<Text>().text = ammoHUD.GetComponent<Text>().text + " / " + clip;
		currentAcc = accuracy;
	}

	void updateWeapon()
	{
		bulletStrength = gun.bulletStrength;
		dmg = gun.dmg;
		RoF = gun.RoF;
		clip = gun.clip;
		projNum = gun.projNum;
		accReduc = gun.accReduc;
		accuracy = gun.accuracy;
		gunShot = gun.gunShot;

		muzzleFlash = gun.muzzleFlash;
		tracer = gun.tracer;

		reload();
	}
	
}


