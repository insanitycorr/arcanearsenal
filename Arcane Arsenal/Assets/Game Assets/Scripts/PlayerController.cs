﻿using UnityEngine;
using UnityEditor;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private GameObject m_camera;




    //Global Speed Variables
    private static float m_speed = 4.0f;
    private static float m_accel = 0.6f;
    private static float m_sneakSpeed = 0.25f;
    private static float m_crouchSpeed = 0.2f;
    private static float m_airAccel = m_accel*0.05f;

    //Jump speed. Currently set so that you jump exactly enough to get over 1 metre.
    private static float m_jumpHeight = 1.2f;
    private static float m_jumpForce = Mathf.Sqrt(2*9.81f*m_jumpHeight);

    //Force to apply in order to stay grounded on sloped terrain.
    private static float m_stickToGroundForce = 1.0f;

    //Maximum ramp angle to prevent sliding on.
    private static float m_maxRampAngle = Mathf.PI/4.0f;







    //Current speed modifier
    private float m_speedMod = 1.0f;

    //Components
    private Rigidbody m_body;
    private CapsuleCollider m_capsule;
    private Animator m_animator;

    //Camera rotation values
    private float camY = 0.0f, camX = 0.0f;

    //Normal of the ground (updated whenever grounded)
    private Vector3 m_groundNormal;

    //Whether or not the player is grounded
    private bool m_isGrounded = true, m_previouslyGrounded = true;

    //If the player is in the air due to jumping
    private bool m_jumping = false;
    private bool m_crouching = false;

    private bool init = false;

    //Update m_isGrounded, and m_previouslyGrounded
    void checkGrounded()
    {
        m_previouslyGrounded = m_isGrounded;
        RaycastHit hitInfo;
        bool g = Physics.SphereCast(transform.position+new Vector3(0.0f,m_capsule.radius + (m_capsule.center.y - 0.91f) * 2.0f), m_capsule.radius * (1.0f - 0.05f), Vector3.down, out hitInfo,
                                   0.05f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        m_groundNormal = hitInfo.normal;
        m_isGrounded = g;
    }

    //Check to see if there is ground directly under the player, and pull the player into it.
    void stickToGround()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position + new Vector3(0.0f, m_capsule.radius + (m_capsule.center.y - 0.91f)*2.0f), m_capsule.radius * (1.0f - 0.4f), Vector3.down, out hitInfo,
                                   0.5f, Physics.AllLayers, QueryTriggerInteraction.Ignore))
        {
            m_body.AddForce(-hitInfo.normal * m_stickToGroundForce, ForceMode.VelocityChange);
            m_isGrounded = true;
        } else
        {
            m_body.velocity = new Vector3(m_body.velocity.x, 0, m_body.velocity.z);
        }
    }

    void Start()
    {
        camX = 0.0f;
        camY = 0.0f;
        m_body = GetComponent<Rigidbody>();
        m_capsule = GetComponent<CapsuleCollider>();
        m_animator = GetComponent<Animator>();
        init = true;
    }

    void OnDrawGizmos()
    {
        if (init)
        {
            Gizmos.DrawWireSphere(transform.position + new Vector3(0.0f, m_capsule.radius + m_capsule.center.y - 0.91f), 0.4f);
        }
    }

    void FixedUpdate()
    {

        #region MouseLook

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        camX += mouseX * Bindings.sensitivity*Mathf.PI/180.0f;
        camY += mouseY * Bindings.sensitivity*Mathf.PI/180.0f;
        
        camY = Mathf.Clamp(camY, -Mathf.PI * 0.25f, Mathf.PI * 0.25f);

        transform.rotation = new Quaternion(0.0f, Mathf.Sin(camX), 0.0f, Mathf.Cos(camX));
        m_camera.transform.rotation = transform.rotation * new Quaternion(Mathf.Sin(camY), 0.0f, 0.0f, Mathf.Cos(camY));

        #endregion

        #region Movement

        checkGrounded();


        //Get Directional Input
        Vector3 directionalInput = new Vector3();
        if (Input.GetKey(Bindings.forward))
        {
            directionalInput.z += 1.0f;
        }
        if (Input.GetKey(Bindings.backward))
        {
            directionalInput.z -= 1.0f;
        }
        if (Input.GetKey(Bindings.right))
        {
            directionalInput.x += 1.0f;
        }
        if (Input.GetKey(Bindings.left))
        {
            directionalInput.x -= 1.0f;
        }
        directionalInput.Normalize();

        //Stay grounded.
        if (m_previouslyGrounded && !m_isGrounded && !m_jumping)
        {
            stickToGround();
        }

        //Handle crouching
        if (m_crouching)
        {
            RaycastHit hit;
            if (!Physics.SphereCast(transform.position + new Vector3(0.0f, 1.0f, 0.0f), m_capsule.radius, Vector3.up, out hit, 1.0f, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                m_crouching = Input.GetKey(Bindings.crouch);
            }
        }
        else
        {
            m_crouching = Input.GetKey(Bindings.crouch);
        }
        m_animator.SetBool("Crouch", m_crouching);
        
        if (m_isGrounded)
        {
            //Stop sliding down ramps.

            float cosRampAngle = Vector3.Dot(Vector3.up.normalized, m_groundNormal.normalized);
            float rampAngle = Mathf.Acos(cosRampAngle);
            if (rampAngle < m_maxRampAngle)
            {
                //This gets the normal force (which is what is pushing you down the ramp), takes only the horizontal components, and applies the inverse,
                //which makes the normal force straight-up, but it's still the wrong magnitude (should be equal and opposite to gravity to reach equillibrium.
                //So add in the remaining force neccesary to equilise it.
                Vector3 fn = -Physics.gravity.y * cosRampAngle * m_groundNormal.normalized;
                float fny = fn.y;
                fn.y = 0.0f;
                m_body.AddForce(-fn, ForceMode.Acceleration);
                m_body.AddForce(0.0f, -Physics.gravity.y - fny, 0.0f, ForceMode.Acceleration);
            }


            m_jumping = false;

            //On ground
            m_body.drag = 10f;

            
            if (m_crouching) m_speedMod = m_crouchSpeed; else if (Input.GetKey(Bindings.sneak)) m_speedMod = m_sneakSpeed; else m_speedMod = 1.0f;

            //Apply walk speed
            Vector3 desiredMove = (directionalInput.z * transform.forward + directionalInput.x * transform.right);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, m_groundNormal).normalized;
            m_body.AddForce(desiredMove * m_accel * m_speedMod, ForceMode.VelocityChange);

            

            //Jump
            if (Input.GetKeyDown(Bindings.jump))
            {
                m_body.velocity = new Vector3(m_body.velocity.x, m_jumpForce, m_body.velocity.z);
                m_jumping = true;
            }
        } else
        {
            //In air
            m_body.drag = 0f;

            //Apply air move speed, but only if under speed cap
            Vector3 desiredMove = (directionalInput.z * transform.forward + directionalInput.x * transform.right) * m_airAccel;
            m_body.AddForce(desiredMove, ForceMode.VelocityChange);
        }
        //Cap horizontal speed
        Vector3 temp = m_body.velocity;
        temp.y = 0.0f;
        if (temp.magnitude > m_speed * m_speedMod)
        {
            m_body.velocity = new Vector3(0.0f, m_body.velocity.y, 0.0f) + temp.normalized * m_speed * m_speedMod;
        }

        #endregion
    }
}
