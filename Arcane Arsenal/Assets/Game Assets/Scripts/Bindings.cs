﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Key/Mouse binds for certain actions.
public class Bindings
{
    public static KeyCode forward = KeyCode.W;
    public static KeyCode backward = KeyCode.S;
    public static KeyCode left = KeyCode.A;
    public static KeyCode right = KeyCode.D;
    public static KeyCode attack0 = KeyCode.Mouse0;
    public static KeyCode attack1 = KeyCode.Mouse1;
    public static KeyCode jump = KeyCode.Space;
    public static KeyCode sneak = KeyCode.LeftShift;
    public static KeyCode crouch = KeyCode.LeftControl;
	public static KeyCode inv1 = KeyCode.Alpha1;
	public static KeyCode inv2 = KeyCode.Alpha2;
	public static KeyCode inv3 = KeyCode.Alpha3;
	public static KeyCode inv4 = KeyCode.Alpha4;
	public static KeyCode inv5 = KeyCode.Alpha5;
	public static KeyCode reload = KeyCode.R;

	public static float sensitivity = 1.0f;
}