﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Guns")]
public class Guns : ScriptableObject
{
	public float accuracy;
	public int accReduc;
	public float dmg;
	public float bulletStrength;
	public float RoF;
	public int clip;
	public int projNum;
	public AudioClip gunShot;

	public ParticleSystem muzzleFlash;
	public ParticleSystem tracer;
}
